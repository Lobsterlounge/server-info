#!/bin/bash

### Script-Version
scriptversion=$"1.1.3"

### Filename
filename=$(hostname)$"_info.txt"

### Linebreak & Underline
lb=$"******************************************************"
ul=$"------------------------------------------------------"

### Get current time and date
time=$(date +%T)
date=$(date +%F)

### Get active PHP version
php_version=$(php -v | head -n 1)

### Get installed PHP-Versions
#php_installed=$(apt list --installed | grep php) # Debian
php_installed=$(zypper search -i -t pattern | grep -E "php[0-9]*" | awk '{print $4" version:"$3}') # SUSE

### Get list of installed PHP modules
php_modules=$(php -m)

### Get active Python version
python_version=$(python3 -V)

### Get mySQL-Version
#mysql_version=$(mysql -u www -pwww -e "SELECT version();" | tail -n 1) # Debian
mysql_version=$(service mysql status) # Suse
mysql_version=$(mariadb --version) # Suse

### Get disk space
disk_space=$(df -h /)

### Get memory info
memory_info=$(cat /proc/meminfo | head -n 1)

### Get CPU info
cpu_info=$(cat /proc/cpuinfo)

### Get server name
server_name=$(hostname)

### Get IP address
ip_address=$(ip a | grep -oP '(?<=inet\s)\d+(\.\d+){3}')

### List CronJobs
cronjobs=$(crontab -l | grep -v "^#")

### Get OS version
#os_info=$(cat /etc/os-release | grep "^NAME" | cut -d '"' -f 2)
os_info=$(cat /etc/os-release | grep "^PRETTY_NAME" | cut -d '"' -f 2)
#os_info=$(cat /etc/os-release)

### Show Server Uptime
uptime=$(uptime)

### Write to CSV file
#echo "PHP Version;PHP Modules;mySQL Version;Disk Space;Memory Info;CPU Info;Server Name;IP Address;OS Version" > $filename
#echo "$php_version;$php_modules;$mysql_version;$disk_space;$memory_info;$cpu_info;$server_name;$ip_address;$os_info" >> $filename
#echo "$php_version","$php_modules","$mysql_version","$disk_space","$memory_info","$cpu_info","$server_name","$ip_address","$os_info" >> $filename

### Write to File
echo "" > $filename
echo "$lb" >> $filename

echo "Server Name:" >> $filename
echo "$server_name;" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Time & Date:" >> $filename
echo "$time, $date" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "PHP-Version:" >> $filename
echo "$php_version" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Installed PHP-Versions:" >> $filename
echo "$php_installed" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "PHP Modules:" >> $filename
echo "$php_modules;" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Python-Version:" >> $filename
echo "$python_version" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Database Version:" >> $filename
echo "$mysql_version" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Disc Space:" >> $filename
echo "$disk_space;" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Memory Info:" >> $filename
echo "$memory_info;" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "CPU Info:" >> $filename
echo "$ul" >> $filename
echo "$cpu_info;" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "IP Addresses:" >> $filename
echo "$ip_address;" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "OS Info:" >> $filename
echo "$os_info" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Server Uptime:" >> $filename
echo "$uptime" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Crons:" >> $filename
echo "$cronjobs" >> $filename
echo "" >> $filename
echo "$lb" >> $filename
echo "" >> $filename

echo "Script-Version: $scriptversion" >> $filename
