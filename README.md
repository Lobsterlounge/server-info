# Server-Info

Skript zur Abfrage diverser Server-Informationen, wie zB. PHP-, und DB-Version, RAM, CPU usw.

## Installation

Auf dem entfernten Server in das gewünschte Verzeichnis wechseln und

````
git clone https://gitlab.com/Lobsterlounge/server-info.git
````

ausführen.

***

## Script anwenden

In das Verzeichnis `/server-info` wechseln und das Script mit `./serverinfo.sh` ausführen.

Es wird eine Text-Datei `<servername>_info.txt` erzeugt, welche die entsprechenden Informationen über den Server enthält.

***

## Skript updaten

Im Verzeichnis des Skriptes
```
git pull
```
ausführen.

***

### Beispiel

```

******************************************************
Server Name:
elwwwti01;

******************************************************

Time & Date:
11:11:32, 2023-02-08

******************************************************

PHP-Version:
PHP 7.4.33 (cli) (built: Nov  3 2022 12:00:00) ( NTS )

******************************************************

Installed PHP-Versions:


******************************************************

PHP Modules:
[PHP Modules]
bz2
Core
ctype
curl
date
dom
fileinfo
filter
gd
gmp
hash
iconv
json
libxml
mbstring
memcached
mysqli
mysqlnd
openssl
pcre
PDO
pdo_mysql
pdo_sqlite
Phar
posix
Reflection
session
SimpleXML
soap
SPL
sqlite3
standard
tokenizer
xml
xmlreader
xmlrpc
xmlwriter
xsl
Zend OPcache
zip
zlib

[Zend Modules]
Zend OPcache;

******************************************************

Python-Version:
Python 3.6.15

******************************************************

Database Version:
mariadb  Ver 15.1 Distrib 10.6.10-MariaDB, for Linux (x86_64) using  EditLine wrapper

******************************************************

Disc Space:
Dateisystem    Größe Benutzt Verf. Verw% Eingehängt auf
/dev/sda2        25G     15G  8,7G   63% /;

******************************************************

Memory Info:
MemTotal:       16033804 kB;

******************************************************

CPU Info:
------------------------------------------------------
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 79
model name	: Intel(R) Xeon(R) CPU E5-2690 v4 @ 2.60GHz
stepping	: 1
microcode	: 0xb00002a
cpu MHz		: 2593.750
cache size	: 35840 KB
physical id	: 0
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts nopl xtopology tsc_reliable nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 invpcid rtm rdseed adx smap xsaveopt arat
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs taa itlb_multihit mmio_stale_data
bogomips	: 5187.50
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:

processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 79
model name	: Intel(R) Xeon(R) CPU E5-2690 v4 @ 2.60GHz
stepping	: 1
microcode	: 0xb00002a
cpu MHz		: 2593.750
cache size	: 35840 KB
physical id	: 2
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 2
initial apicid	: 2
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts nopl xtopology tsc_reliable nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 invpcid rtm rdseed adx smap xsaveopt arat
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs taa itlb_multihit mmio_stale_data
bogomips	: 5187.50
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:

processor	: 2
vendor_id	: GenuineIntel
cpu family	: 6
model		: 79
model name	: Intel(R) Xeon(R) CPU E5-2690 v4 @ 2.60GHz
stepping	: 1
microcode	: 0xb00002a
cpu MHz		: 2593.750
cache size	: 35840 KB
physical id	: 4
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 4
initial apicid	: 4
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts nopl xtopology tsc_reliable nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 invpcid rtm rdseed adx smap xsaveopt arat
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs taa itlb_multihit mmio_stale_data
bogomips	: 5187.50
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:

processor	: 3
vendor_id	: GenuineIntel
cpu family	: 6
model		: 79
model name	: Intel(R) Xeon(R) CPU E5-2690 v4 @ 2.60GHz
stepping	: 1
microcode	: 0xb00002a
cpu MHz		: 2593.750
cache size	: 35840 KB
physical id	: 6
siblings	: 1
core id		: 0
cpu cores	: 1
apicid		: 6
initial apicid	: 6
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts nopl xtopology tsc_reliable nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch cpuid_fault invpcid_single pti fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 invpcid rtm rdseed adx smap xsaveopt arat
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs taa itlb_multihit mmio_stale_data
bogomips	: 5187.50
clflush size	: 64
cache_alignment	: 64
address sizes	: 43 bits physical, 48 bits virtual
power management:;

******************************************************

IP Addresses:
127.0.0.1
172.31.78.170;

******************************************************

OS Info:
SUSE Linux Enterprise Server 15 SP4

******************************************************

Server Uptime:
 11:11:33  42 Tage 23:35 an,  2 Benutzer,  Durchschnittslast: 0,08, 0,04, 0,03

******************************************************

Crons:


******************************************************

Script-Version: 1.1.3

```
